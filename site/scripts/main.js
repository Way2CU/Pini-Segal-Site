/**
 * Main JavaScript
 * Dr.Pini Segal
 *
 * Copyright (c) 2018., 2023. by Way2CU, http://way2cu.com
 * Authors: Tal Reznic, Mladen Mijatov, Vladimir Anusic
 */

// create or use existing site scope
var Site = Site || {};

// make sure variable cache exists
Site.variable_cache = Site.variable_cache || {};


/**
 * Check if site is being displayed on mobile.
 * @return boolean
 */
Site.is_mobile = function() {
	var result = false;

	// check for cached value
	if ('mobile_version' in Site.variable_cache) {
		result = Site.variable_cache['mobile_version'];

	} else {
		// detect if site is mobile
		var elements = document.getElementsByName('viewport');

		// check all tags and find `meta`
		for (var i=0, count=elements.length; i<count; i++) {
			var tag = elements[i];

			if (tag.tagName == 'META') {
				result = true;
				break;
			}
		}

		// cache value so next time we are faster
		Site.variable_cache['mobile_version'] = result;
	}

	return result;
};

/**
 * Function called when document and images have been completely loaded.
 */
Site.on_load = function() {
	if (Site.is_mobile())
		Site.mobile_menu = new Caracal.MobileMenu();

	// function for home page slider
	Site.slider = new PageControl('section#slider', 'figure');
	Site.slider
		.setInterval(6000)
		.setWrapAround(true);

	// create function for rotating between testimonials
	Site.testimonials = new PageControl('section#testimonial', 'article');
	Site.testimonials
		.setInterval(10000)
		.setWrapAround(true);

	// create lightbox for about us page gallery
	if (document.querySelector('div.gallery_container')) {
		Site.lightbox = new Caracal.Gallery.LightBox();
		Site.lightbox.images.add(document.querySelectorAll('div.gallery_container a'));
	}
};

// connect document `load` event with handler function
window.addEventListener('load', Site.on_load);
